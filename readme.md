Projet personnel: une plateforme web qui permet la création de dossiers de compétences en ligne.
Technos utilisés : 
- PHP (Laravel)
- Angular 10
### But du projet
- Chaque employé d'une entreprise crée un ou plusieurs dossiers.
- Chaque responsable ou manager d'une des agences de son entreprise aura accès en lecture à tous les dossiers de son agence.
- Les dossiers de compétences pourront être téléchargés en format pdf
- Possibilité de partager les dossiers avec les entreprises qui recherche des sous traitants

### BDD Conception
![](./bdd.PNG);

